# Pull image 
FROM openjdk:8-jre-alpine

COPY ./jstl-1.2.jar  jstl-1.2.jar

EXPOSE 8091

CMD java -jar jstl-1.2.jar


