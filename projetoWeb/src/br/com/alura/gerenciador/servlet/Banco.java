package br.com.alura.gerenciador.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Banco {
	
	private Connection conn;
	
	private static List<CadastroNome> lista = new ArrayList<CadastroNome>();
	
	public void adiciona(CadastroNome nome) throws IOException, SQLException {
		
		conn = DB.getConnection();
		PreparedStatement st = conn.prepareStatement("INSERT INTO teste(Nome, UltimoNome) VALUES(?,?)");
		
		try {
			st.setString(1, nome.getNome());
			st.setString(2, nome.getUltimoNome());
			
			st.execute();
			}
		catch(SQLException e) {
			throw new DbException("Erro Inexperado");
		}
		
		finally {
			DB.closeStatement(st);
		}
//		try(FileWriter fw = new FileWriter("C:\\Users\\rafael.costa\\teste.txt",true)){
//			fw.write(nome.toString() + "\n");
//			fw.flush();
//		}
		//lista.add(nome);
	}
	
//	public void removeNome(Integer id) { 

//	    Iterator<CadastroNome> it = lista.iterator();
//
//	    while(it.hasNext()) { 
//	        CadastroNome cn = it.next();
//
//	        if(cn.getId() == id ) {
//	            it.remove();
//	        }
//	    }
//	}
	
	public List<CadastroNome> getNomes(){
		return Banco.lista;
	}

//	public CadastroNome buscaNomePelaId(Integer id) {
//		for (CadastroNome nome : lista) {
//			if(nome.getId() == id) {
//				return nome;
//			}
//		}
//		return null;
//	}
}