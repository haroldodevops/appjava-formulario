package br.com.alura.gerenciador.servlet;

public class CadastroNome {
	
	private String nome;
	private String ultimoNome;
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getUltimoNome() {
		return ultimoNome;
	}

	public void setUltimoNome(String ultimoNome) {
		this.ultimoNome = ultimoNome;
	}

	@Override
	public String toString() {
		return "Nome: " + nome + ", Sobrenome: " + ultimoNome;
	}
	
	
}