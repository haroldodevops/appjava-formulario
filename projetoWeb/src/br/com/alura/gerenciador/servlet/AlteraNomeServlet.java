package br.com.alura.gerenciador.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AlteraEmpresaServlet
 */
@WebServlet("/alteraNome")
public class AlteraNomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AlteraNomeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("Alterando Nome");
		
		String novoNome = request.getParameter("nome");
		String ultimoNome = request.getParameter("ultimoNome");
//		String paramId = request.getParameter("id");
//		Integer id = Integer.valueOf(paramId);
//		System.out.println(id);
		
		Banco banco = new Banco();
//		CadastroNome nome = banco.buscaNomePelaId(id);
//		nome.setNome(novoNome);
//		nome.setUltimoNome(ultimoNome);
		
		response.sendRedirect("listaNomes");
	}

}
