package br.com.alura.gerenciador.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DB {
	
	//private static String NAME = "com.mysql.jdbc.Driver";
	private static String URL = "jdbc:mysql://192.168.56.101:3306/hello";
	private static String LOGIN = "root";
	private static String PASS = "1234";

	public static Connection getConnection() throws SQLException, IOException {

		Connection con = null;

		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			con = DriverManager.getConnection(URL, LOGIN, PASS);
		} catch (ClassNotFoundException e) {
			System.out.print("\nN�o foi poss�vel estabelecer conex�o com a base de dados.\n");
			e.printStackTrace();
			return null;
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return con;
	}
	public static void closeStatement(Statement st) {

		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				throw new DbException(e.getMessage());
			}
		}
	}
}
//	public static Connection getConnection() {
//		if (conn == null || conn != null) {
//			try {
//				Class.forName("com.mysql.jdbc.Driver");
//				Properties props = loadProperties();
//				String url = props.getProperty("dburl");
//				conn = DriverManager.getConnection(url, "root" , "1234");
//				
//				//conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/comando", "Rafael" , "Rafa2905");
//			}
//			catch (SQLException e) {
//				throw new DbException(e.getMessage());
//			} catch (ClassNotFoundException e) {
//				e.printStackTrace();
//			}
//		}
//		return conn;
//	}
//	
//	public static void closeConnection() {
//		if (conn != null) {
//			try {
//				conn.close();
//			}
//			catch (SQLException e) {
//				throw new DbException(e.getMessage());
//			}
//		}
//	}
//
//	private static Properties loadProperties() {
//		try (FileInputStream fs = new FileInputStream("C:\\Users\\rafael.costa\\Desktop\\Ultimo Dia\\alura-java-servlet\\gerenciador\\db.properties")) {
//			Properties props = new Properties();
//			props.load(fs);
//			return props;
//		} catch (IOException e) {
//			throw new DbException(e.getMessage());
//		}
//	}
//	
//	public static void closeStatement(Statement st) {
//
//		if (st != null) {
//			try {
//				st.close();
//			} catch (SQLException e) {
//				throw new DbException(e.getMessage());
//			}
//		}
//	}
