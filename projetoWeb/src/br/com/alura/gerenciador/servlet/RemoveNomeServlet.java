//package br.com.alura.gerenciador.servlet;
//
//import java.io.IOException;
//
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//@WebServlet("/removeNome")
//public class RemoveNomeServlet extends HttpServlet {
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 1L;
//	@Override
//	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		
//	   	String paramId = request.getParameter("id");
//    	Integer id = Integer.valueOf(paramId);
//
//    	System.out.println(id);
//
//    	Banco banco = new Banco();
//    	banco.removeNome(id);
//
//    	response.sendRedirect("listaEmpresas");
//	}
//	
//}