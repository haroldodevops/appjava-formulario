%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List, br.com.alura.gerenciador.servlet.CadastroNome" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Java Standard Taglib</title>
</head>
<body>
	
	<c:if test="${not empty nome}">
		Nome ${ nome } cadastrado com sucesso!
	</c:if>
	
	Lista de nomes: <br />
	
	<ul>
		<c:forEach items="${ nomes }" var="nome">
			<li>
			${ nome.nome } - ${ nome.ultimoNome }
			<a href="/projetoWeb/mostraNome?id=${nome.id }">editar</a>
			<a href="/projetoWeb/removeNome?id=${nome.id }">remover</a>
			</li>
		</c:forEach>
	</ul>
</body>
</html>